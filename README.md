# google-sheets-telegram-notification

This server app consists of two parts:

1. Take and process data from Google Sheets using the Google Sheets API;
2. This is Telegram bot that interacts with the user and sends him data from the tables.

`npm run server` - command to build and run server.