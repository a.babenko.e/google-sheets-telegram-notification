const gulp   =  require('gulp'),
      rename = require('gulp-rename'),
      uglify = require('gulp-uglify'),
      babel  = require('gulp-babel');

const path = {
    build: { // The path to send files
        js: './',
    },
    src: { // Path to source files
        js: 'src/*.js',
    },
    watch: { // Files to be watched
        js: 'src/*.js',
    }
};

gulp.task('js', function() {
    return gulp.src(path.src.js)
        .pipe(babel({presets: ['@babel/env']}))
        .pipe(uglify())
        .pipe(rename({ basename: 'server' }))
        .pipe(gulp.dest(path.build.js));
});

gulp.task('watch', function() {
    gulp.watch(path.watch.js, 'js');
});

gulp.task('default', function() {
    gulp.start('js');
});
