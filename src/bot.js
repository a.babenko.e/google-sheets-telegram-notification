(function () {
    'use strict';
    require('dotenv').config();

    // Import modules
    const fs = require('fs'),
        request = require('request'),
        readline = require('readline'),
        google = require('googleapis'),
        moment = require('moment-timezone'),
        googleAuth = require('google-auth-library'),
        TelegramBot = require('node-telegram-bot-api'),
        appData = require('./data/appData.json'),
        /**
         * Webhook config
         * @todo move this vars into separate file
         */
        port = process.env.PORT,
        host = process.env.HOST,
        externalUrl = process.env.EXTERNAL_URL;
        // Bot initialization and webhook obj declarating
    let bot;

    if (process.env.MODE === "DEVELOPMENT") {
        bot = new TelegramBot(process.env.TOKEN, {
            polling: true
        });
    } else {
        bot = new TelegramBot(
            process.env.TOKEN,
            { webHook: { port : port, host : host } }
        );
        bot.setWebHook(`${externalUrl}:${port}/bot${process.env.TOKEN}`);
    }

    const credsRequestOptions = {
        method: 'GET',
        url: process.env.CREDS_URL,
        headers: { 'cache-control': 'no-cache', 'x-apikey': process.env.DB_API_KEY },
    };

    const tokenRequestOptions = {
        method: 'GET',
        url: process.env.TOKEN_URL,
        headers: { 'cache-control': 'no-cache', 'x-apikey': process.env.DB_API_KEY },
    };

    // Token config vars
    const SCOPES = ["https://www.googleapis.com/auth/spreadsheets.readonly"],
        TOKEN_PATH = appData.config.pathToSheetsToken,
        TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/.credentials/';

    // Sheet and events config vars
    // Diapason of data cells
    const dataRange = appData.config.sheetDataRange,
        listName = appData.config.sheetList,
        sheetUrl = process.env.SHEET_URL,
        eventsData = [{
            eventName: appData.events.school.name,
            // Monday By UA time
            eventDay: appData.events.school.day,
            eventHour: appData.events.school.hour
        },
        {
            eventName: appData.events.public.name,
            // Saturday by UA time
            eventDay: appData.events.public.day,
            eventHour: appData.events.public.hour
        }
        ],

        phrases = appData.phrases.time;

    console.log(`Sheets URL: ${sheetUrl}`);

    const optsFastReply = {
        reply_markup: JSON.stringify({
            force_reply: true
        })
    };

    const initialOptions = {
        parse_mode: 'Markdown',
        resize_keyboard: true,
        reply_markup: JSON.stringify({
            keyboard: [
                [{
                    text: appData.keyboard.regName.text,
                    callback_data: appData.keyboard.regName.callbackData
                }]
            ]
        })
    };

    const options = {
        parse_mode: 'Markdown',
        resize_keyboard: true,
        reply_markup: JSON.stringify({
            keyboard: [
                [{
                    text: appData.keyboard.whenIWhitnessing.text,
                    callback_data: appData.keyboard.whenIWhitnessing.callbackData
                },
                {
                    text: appData.keyboard.goToTable.text,
                    callback_data: appData.keyboard.goToTable.callbackData
                }
                ]
            ]
        })
    };

    const plainOptions = {
        parse_mode: 'Markdown'
    };

    let authData;

    bot.onText(/\/start/, function onPhotoText(msg) {
        const chatId = msg.chat.id;

        bot.sendMessage(
            chatId,
            appData.phrases.common.helloMessage,
            plainOptions
        );

        setTimeout(() => {
            bot.sendMessage(
                chatId,
                appData.phrases.common.askQuestions,
                plainOptions
            );
        }, 2000);

        setTimeout(() => {
            bot.sendMessage(
                chatId,
                appData.phrases.common.registerYourName,
                initialOptions
            );
        }, 4000);
    });

    bot.onText(/Зарегестрировать моё имя/, function onPhotoText(msg) {
        bot.sendMessage(
            msg.chat.id,
            appData.phrases.common.writeYourName,
            optsFastReply
        ).then(function (sended) {
            let chatId = sended.chat.id;
            let messageId = sended.message_id;
            bot.onReplyToMessage(chatId, messageId, function (message) {
                message.from.nameInTable = message.text;
                message.from.chatId = message.chat.id;
                addNewUser(message);
            });
        });
    });

    bot.onText(/\/getBoard/, function onPhotoText(msg) {
        bot.sendMessage(msg.chat.id, appData.phrases.common.pickOption, options);
    });

    bot.onText(/Посмотреть когда я служу/, function onPhotoText(msg) {
        getCurrentEvents().then((closerEventsData) => {
            sendCurrentEventsData(closerEventsData, msg.chat.id, msg.from.id);
        });
    });

    bot.onText(/Перейти к таблице/, function onPhotoText(msg) {
        bot.sendMessage(msg.chat.id, `[${appData.phrases.common.clickToTable}](${sheetUrl})`, options);
    });

    /**
     * Get local time
     */
    function getLocalTime() {
        return moment().tz('Europe/Kiev');
        // return moment([2018, 5, 16, 16, 59, 59]);
    }

    function setNotificationSendingTime() {
        let currentDate = getLocalTime(),
            currentDay = currentDate.day(),
            currentHour = currentDate.hour(),
            closerEvent =
                getCloserDayOfWeek(currentDay, eventsData[0].eventDay, eventsData[1].eventDay) ===
                    eventsData[0].eventDay ?
                    eventsData[0] :
                    eventsData[1];

        if (closerEvent.eventDay === currentDay && currentHour > closerEvent.eventHour) {
            closerEvent = closerEvent.eventDay === eventsData[0].eventDay ? eventsData[1] : eventsData[0];
        }

        if (closerEvent.eventDay === currentDay) {
            if (
                getLocalTime()
                    .add(2, 'hours')
                    .isSameOrBefore(getLocalTime().hour(closerEvent.eventHour))
            ) {
                // Event in this day. Notify 2 hours before
                closerEvent.phrase = phrases.twoHours;
                setDelayedNotificationSending(0, 2, closerEvent);
            } else {
                // If event already passed in this day
                setTimeout(
                    () => {
                        setNotificationSendingTime();
                    },
                    getLocalTime()
                        .hours(25)
                        .millisecond()
                );
            }
        } else {
            if (getDaysNumToEvent(closerEvent.eventDay) >= 2) {
                if (getDaysNumToEvent(closerEvent.eventDay) === 2) {
                    // It is 2 or more days before closer event
                    if (getLocalTime().hours() >= closerEvent.eventHour) {
                        closerEvent.phrase = phrases.oneDay;
                        setDelayedNotificationSending(1, 2, closerEvent);
                    } else {
                        closerEvent.phrase = phrases.twoDays;
                        setDelayedNotificationSending(0, 2, closerEvent);
                    }
                } else {
                    if (getLocalTime().hours() >= closerEvent.eventHour) {
                        closerEvent.phrase = phrases.oneDay;
                        setDelayedNotificationSending(getDaysNumToEvent(closerEvent.eventDay) - 1, 2, closerEvent);
                    } else {
                        closerEvent.phrase = phrases.twoDays;
                        setDelayedNotificationSending(getDaysNumToEvent(closerEvent.eventDay) - 2, 2, closerEvent);
                    }
                }
            } else {
                // 1 day before event
                if (getLocalTime().hours() >= closerEvent.eventHour) {
                    closerEvent.phrase = phrases.twoHours;
                    setDelayedNotificationSending(1, 2, closerEvent);
                } else {
                    closerEvent.phrase = phrases.oneDay;
                    setDelayedNotificationSending(0, 2, closerEvent);
                }
            }
        }
    }

    /**
     * @param {number} daysBefore - specifies how many days before the event to set a notification sending.
     * @param {number} hoursBefore - how many hours before the event make the event a reminder.
     * @param {object} closerEventData - object with data about the nearest event.
     */
    function setDelayedNotificationSending(daysBefore, hoursBefore, closerEventData) {
        let notificationDate = getLocalTime();

        notificationDate.minutes(0);
        notificationDate.seconds(0);
        notificationDate.hours(closerEventData.eventHour - hoursBefore);
        notificationDate.day(getLocalTime().day() + daysBefore);

        let timeout = Math.abs(getLocalTime().diff(notificationDate, 'milliseconds'));

        console.log(
            'Notification setted to: ' +
            getLocalTime()
                .add(timeout, 'milliseconds')
                .toString()
        );

        console.log('Closer event data:' + JSON.stringify(closerEventData));


        console.log('Timeout: ' + timeout);

        setTimeout(() => {
            sendNotifications(closerEventData);
            setTimeout(() => {
                setNotificationSendingTime();
            }, Math.abs(getLocalTime().diff(getLocalTime().hour(25))));
        }, timeout);
    }

    /**
     * @param {number} closerEventDay - event day in diapason from 0 to 6.
     *
     * @returns {number} - diffetence between current day and event day from 0 to 6.
     */
    function getDaysNumToEvent(closerEventDay) {
        if (getLocalTime().isSameOrBefore(getLocalTime().day(closerEventDay))) {
            return getLocalTime()
                .day(closerEventDay)
                .diff(getLocalTime(), 'day');
        } else {
            return getLocalTime()
                .day(closerEventDay + 7)
                .diff(getLocalTime(), 'day');
        }
    }

    /**
     * @param {Object} eventData - closer event data.
     */
    function sendNotifications(eventData = false) {
        if (eventData) {
            getUsersData().then((res) => {
                res.forEach(item => {
                    let userData = item;

                    getCurrentEvents().then((closerEventsData) => {
                        sendCurrentEventsData(closerEventsData, userData.chatId, userData.id, eventData);
                    });
                });
            });
        } else {
            getUsersData().then((res) => {
                res.forEach(item => {
                    let userData = item;

                    getCurrentEvents().then((closerEventsData) => {
                        sendCurrentEventsData(closerEventsData, userData.chatId, userData.id);
                    });
                });
            });
        }
    }

    function getCloserDayOfWeek(currentDay, firstDay, secondDay) {
        for (let i = currentDay; ; i++) {
            if (i === firstDay) {
                return firstDay;
            } else if (i === secondDay) {
                return secondDay;
            }

            if (i === 6) {
                i = -1;
            }
        }
    }

    /**
     * Get users data from database.
     */
    function getUsersData() {
        return new Promise((resolve) => {
            let options = {
                method: 'GET',
                url: process.env.DB_API_URL,
                headers: {
                    'cache-control': 'no-cache',
                    'x-apikey': process.env.DB_API_KEY
                }
            };

            request(options, function (error, response, body) {
                if (error) {
                    throw new Error(error);
                }

                resolve(JSON.parse(body));
            });

            // resolve([{
            //     _id: '5b1336f62d16123c0000e272',
            //     id: 382481521,
            //     is_bot: false,
            //     first_name: 'Anton',
            //     last_name: 'Babenko',
            //     username: 'tonnio',
            //     language_code: 'ru-RU',
            //     chatId: 382481521,
            //     nameInTable: 'Бабенко'
            // }]);
        });
    }

    /**
     * If user name exist in table then add user data to database.
     *
     * @param {Object} userData - telegram user data including user name in table.
     */
    function addNewUser(userData) {
        isNameExistInTable(userData.from.nameInTable).then((isExist) => {
            if (isExist) {
                let reqOptions = {
                    method: 'POST',
                    url: process.env.DB_API_URL,
                    headers: {
                        'cache-control': 'no-cache',
                        'x-apikey': process.env.DB_API_KEY,
                        'content-type': 'application/json'
                    },
                    body: userData.from,
                    json: true
                };

                request(reqOptions, function (error, response, body) {
                    if (error) throw new Error(error);

                    bot.sendMessage(
                        userData.from.chatId,
                        appData.phrases.common.foundNameInTable,
                        options
                    );
                });
            } else {
                bot.sendMessage(
                    userData.from.chatId,
                    appData.phrases.common.notFoundNameInTable,
                    initialOptions
                );
            }
        });
    }

    /**
     * Checks whether there is a name that the user entered in the table.
     *
     * @param {string} name - user name from table.
     */
    function isNameExistInTable(name) {
        return new Promise((resolve) => getSheetValues().then((res) => {
            for (let i = 0; i < res.length; i++) {
                for (let x = 0; x < res[i].length; x++) {
                    if (name === res[i][x]) {
                        resolve(true);
                        return true;
                    }
                }
            }

            resolve(false);
        }));
    }

    function buildMessage(date, performer, point, event, altPhrase = false) {
        if (altPhrase) {
            return `Дорогой брат *${performer}*, на встрече через *${altPhrase}* ты ответственнен за: \n• ${point}`;
        } else {
            return `Дорогой брат *${performer}*, \nна неделе от *${date}* ты ответственнен за: \n\n*${event}*\n• ${point}`;
        }
    }

    function updateMessage(msg, point, event, prevEvent, currDate, prevDate, ignoreNewEvent) {
        let currEvent = event === prevEvent && currDate === prevDate ? '' : event ? `\n*${event}*\n` : '';
        let newDateMsg =
            currDate === prevDate ? '' : currDate ? `\nНа неделе от *${currDate}* ты ответственнен за:\n` : '';
        if (!ignoreNewEvent) {
            return `${msg}\n${newDateMsg}${currEvent}• ${point}`;
        } else {
            return msg;
        }
    }

    function sendCurrentEventsData(sheetsData, chatID, id, eventData = false) {
        getFullMessage(sheetsData, id, eventData).then((msg) => {
            bot.sendMessage(chatID, msg, options);
        });
    }

    function getDataById(id) {
        return getUsersData().then((res) => {
            for (let i = 0; i < res.length; i++) {
                const element = res[i];

                if (element.id === id) {
                    return element;
                }
            }
        });
    }

    /**
     * @param {Object} sheetsData - closer events data.
     * @param {number} id - user telegram id.
     * @param {*} eventData - closer event data (for automatic notification).
     *
     * @returns - return ready to send msg that includes data for closer events.
     */
    function getFullMessage(sheetsData, id, eventData) {
        return new Promise((resolve) => {

            let isAlreadyProcessed = false;
            let msg = false;
            let perfData = getDataById(id) || false;
            let prevEvent;
            let prevDate;

            perfData.then((performerData) => {
                if (id) {
                    if (!eventData) {
                        sheetsData.forEach(item => {
                            let itemEventData = item.event === appData.events.school.name ? appData.events.school : appData.events.public;

                            if (getLocalTime()
                                .month(item.eventMonth - 1)
                                .date(item.eventDay)
                                .day(itemEventData.day)
                                .hour(itemEventData.hour)
                                .minutes(0)
                                .isAfter(getLocalTime())) {
                                if (isNameExist(performerData, item.performer)) {
                                    if (!isAlreadyProcessed) {
                                        prevEvent = item.event;
                                        prevDate = `${item.eventDay}.${item.eventMonth}`;

                                        msg = buildMessage(prevDate, item.performer, item.point, item.event);

                                        isAlreadyProcessed = true;
                                    } else {
                                        msg = updateMessage(
                                            msg,
                                            item.point,
                                            item.event,
                                            prevEvent,
                                            `${item.eventDay}.${item.eventMonth}`,
                                            prevDate
                                        );
                                        prevEvent = item.event;
                                        prevDate = `${item.eventDay}.${item.eventMonth}`;
                                    }
                                }
                            }
                        });
                    } else {
                        sheetsData.forEach(item => {
                            if (
                                eventData.eventName === item.event &&
                                getLocalTime()
                                    .month(item.eventMonth - 1)
                                    .date(item.eventDay)
                                    .day(eventData.eventDay)
                                    .hour(eventData.eventHour)
                                    .minutes(0)
                                    .isAfter(getLocalTime()) &&
                                getLocalTime()
                                    .month(item.eventMonth - 1)
                                    .date(item.eventDay)
                                    .day(eventData.eventDay)
                                    .hour(eventData.eventHour)
                                    .minutes(0)
                                    .isBefore(getLocalTime().day(getLocalTime().day() + 3))
                            ) {
                                if (isNameExist(performerData, item.performer)) {
                                    if (!isAlreadyProcessed) {
                                        prevEvent = item.event;
                                        prevDate = `${item.eventDay}.${item.eventMonth}`;

                                        msg = buildMessage(prevDate, item.performer, item.point, item.event, eventData.phrase);

                                        isAlreadyProcessed = true;
                                    } else {
                                        msg = updateMessage(
                                            msg,
                                            item.point,
                                            item.event,
                                            prevEvent,
                                            `${item.eventDay}.${item.eventMonth}`,
                                            prevDate
                                        );
                                        prevEvent = item.event;
                                        prevDate = `${item.eventDay}.${item.eventMonth}`;
                                    }
                                }
                            }
                        });
                    }

                    if (!msg) {
                        if (!eventData) {
                            msg = appData.phrases.common.notFoundAppointments;
                        } else {
                            msg = appData.phrases.common.notFoundCloserAppointments;
                        }
                    }
                    resolve(msg);
                }
            });
        });
    }

    function isNameExist(perfData, performer) {
        if (performer === perfData.nameInTable) {
            return true;
        }
        return false;
    }

    /**
     * Google Sheets
     */

    /**
     * Get raw sheets data.
     *
     * @returns {Array} - sheets data.
     */
    function getSheetValues() {
        return new Promise((resolve, reject) => {
            const sheets = google.sheets('v4');
            sheets.spreadsheets.values.get({
                auth: authData,
                spreadsheetId: process.env.SHEET_ID,
                range: `${listName}!${dataRange}`
            },
                function (err, response) {
                    if (err) {
                        reject();
                        console.log('The API returned an error: ' + err);
                        return;
                    } else {
                        resolve(response.values);
                    }
                });
        });
    }

    /**
     * Get current events data.
     */
    function getCurrentEvents() {
        return getSheetValues().then((rows) => {
            if (rows.length === 0) {
                console.log('No data found.');
            } else {
                let currentDate = getLocalTime();

                let currMonth = currentDate.month() + 1;
                let currDate = currentDate.date();

                let datesRow = rows[1];

                let actualDates = [];
                let actualDateIndexs = [];

                const appointments = [];

                for (let i = 0; i < datesRow.length; i++) {
                    let el = datesRow[i].split('/');

                    if ((+el[1] === currMonth && +el[0] > currDate) || (+el[1] === currMonth + 1 && +el[0])) {
                        actualDateIndexs.push(datesRow.indexOf(datesRow[i - 1]));
                        actualDates.push(datesRow[i - 1].split('/'));
                        actualDateIndexs.push(datesRow.indexOf(datesRow[i]));
                        actualDates.push(el);
                        break;
                    }
                }

                actualDateIndexs.forEach((item, x) => {
                    for (let i = 4; i < 35; i++) {
                        if (!rows[i][item] || rows[i][item] === '---' || rows[i][item] === ' ') {
                            continue;
                        }

                        if (i < 24 || i > 24) {
                            appointments.push({
                                point: rows[i][0],
                                performer: rows[i][item],
                                eventDay: actualDates[x][0],
                                eventMonth: actualDates[x][1],
                                event: i < 24 ?
                                    appData.events.school.name : i > 24 ? appData.events.public.name : appData.events.unknown
                            });
                        }

                    }
                });

                return appointments;
            }
        });
    }

    /**
     * Load client secrets from a local file.
     */
    // fs.readFile(appData.config.pathToClientSecret, function processClientSecrets(err, content) {
    //     if (err) {
    //         console.log('Error loading client secret file: ' + err);
    //         return;
    //     }
    //
    //     authorize(JSON.parse(content));
    // });

    request(credsRequestOptions, (err, res, body) => {
        authorize(JSON.parse(body)[0]);
    });

    /**
     * Create an OAuth2 client with the given credentials, and then execute the
     * Given callback function.
     *
     * @param {Object} credentials - The authorization client credentials.
     */
    function authorize(credentials) {
        var clientSecret = credentials.client_secret;
        var clientId = credentials.client_id;
        var redirectUrl = credentials.redirect_uris[0];
        var auth = new googleAuth();
        var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
        authData = oauth2Client;

        // Check if we have previously stored a token.
        fs.readFile(TOKEN_PATH, function (err, token) {
            if (err) {
                getNewToken(oauth2Client);
            } else {
                oauth2Client.credentials = JSON.parse(token);
            }
        });
    }

    /**
     * Get and store new token after prompting for user authorization, and then
     * execute the given callback with the authorized OAuth2 client.
     *
     * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
     * @param {getEventsCallback} callback The callback to call with the authorized client.
     */
    function getNewToken(oauth2Client, callback) {
        var authUrl = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES
        });

        console.log('Authorize this app by visiting this url: ', authUrl);
        var rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        rl.question('Enter the code from that page here: ', function (code) {
            rl.close();
            oauth2Client.getToken(code, function (err, token) {
                if (err) {
                    console.log('Error while trying to retrieve access token', err);
                    return;
                }
                oauth2Client.credentials = token;
                storeToken(token);
                callback(oauth2Client);
            });
        });
    }

    /**
     * Store token to disk be used in later program executions.
     *
     * @param {Object} token The token to store to disk.
     */
    function storeToken(token) {
        try {
            fs.mkdirSync(TOKEN_DIR);
        } catch (err) {
            if (err.code !== 'EXIST') {
                throw err;
            }
        }
        fs.writeFile(TOKEN_PATH, JSON.stringify(token));
        console.log('Token stored to ' + TOKEN_PATH);
    }
})();
